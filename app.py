from flask import Flask, url_for, session, request
from flask import render_template, redirect
from authlib.integrations.flask_client import OAuth
import discord
import os
import requests
import json
import jwt
import datetime

## LINK : https://discord.com/oauth2/authorize?client_id=1066514095404236800&scope=bot&permissions=268435475

DISCORD_CLIENT_ID = os.environ.get('DISCORD_CLIENT_ID')
DISCORD_CLIENT_SECRET = os.environ.get('DISCORD_CLIENT_SECRET')

DISCORD_BOT_TOKEN = os.environ.get('DISCORD_BOT_TOKEN')
SERVER_ID = os.environ.get('SERVER_ID')

DISCORD_ROLE_1 = os.environ.get('DISCORD_ROLE_1')
DISCORD_ROLE_2 = os.environ.get('DISCORD_ROLE_2')
DISCORD_CHANNEL_ID = os.environ.get('DISCORD_CHANNEL_ID')

SMTP_SERVER = os.environ.get('SMTP_SERVER', False)
SMTP_USER = os.environ.get('SMTP_USER')
SMTP_PASSWORD = os.environ.get('SMTP_PASSWORD')
SMTP_PORT = os.environ.get('SMTP_PORT')

APP_SECRET = os.environ.get('APP_SECRET')

app = Flask(__name__)
app.secret_key = APP_SECRET
app.config.from_object('config')

CONF_URL = 'https://accounts.google.com/.well-known/openid-configuration'
oauth = OAuth(app)
oauth.register(
    name='google',
    server_metadata_url=CONF_URL,
    client_kwargs={
        'scope': 'openid email profile'
    }
)

oauth.register(
    name='discord',
    client_id=DISCORD_CLIENT_ID,
    client_secret=DISCORD_CLIENT_SECRET,
    access_token_url="https://discord.com/api/oauth2/token",
    access_token_params=None,
    authorize_url="https://discord.com/oauth2/authorize",
    authorize_params=None,
    client_kwargs={
        'scope': 'guilds.join identify'
    }
    
)

def add_to_guild(access_token, userID):
    url = f"https://discord.com/api/guilds/{SERVER_ID}/members/{userID}"
    data = {
        "access_token" : access_token,
    }
    headers = {
        "Authorization" : f"Bot {DISCORD_BOT_TOKEN}",
        'Content-Type': 'application/json'
    }
    response = requests.put(url=url, headers=headers, json=data)

def add_to_role(access_token, userID, roleID):
    url = f"https://discord.com/api/guilds/{SERVER_ID}/members/{userID}/roles/{roleID}"
    data = {
        "access_token" : access_token,
    }
    headers = {
        "Authorization" : f"Bot {DISCORD_BOT_TOKEN}",
        'Content-Type': 'application/json'
    }
    response = requests.put(url=url, headers=headers, json=data)

def send_message(userID, message):
    url = "https://discord.com/api/channels/{}/messages".format(DISCORD_CHANNEL_ID)
    headers = {
        "Authorization" : f"Bot {DISCORD_BOT_TOKEN}",
        'Content-Type': 'application/json'
    }
    data = {
        "content": f"<@{userID}>, you've been verified!",
        "embeds": [{
            "title": ":white_check_mark: User Verified",
            "description": message
        }]
    }
    response = requests.post(url=url, headers=headers, json=data)

@app.route('/')
def homepage():
    pass_gauth = session.get('pass_gauth', None)
    pass_dauth = session.get('pass_dauth', None)
    if pass_gauth and pass_dauth:
        return redirect('/welcome')
    if pass_gauth and pass_dauth == None:
        return redirect('/step2')
    return render_template('home.html')

@app.route('/noroute')
def noroute():
    return render_template('noroute.html')

@app.route('/welcome')
def welcome():
    pass_gauth = session.get('pass_gauth', None)
    pass_dauth = session.get('pass_dauth', None)
    if pass_gauth and pass_dauth:
        return render_template('welcome.html')
    else:
        return redirect('/') 

@app.route('/export')
def export():
    if 'imported' in session and session['imported']:
        return redirect('/step2')   
    else:
        pass_gauth = session.get('pass_gauth', None)
        googler = session.get('googler', None)
        loogler = session.get('loogler', None)
        if pass_gauth:
            payload = {
                'g_verified': True,
                'googler': googler or False,
                'loogler': loogler or False,
                'exp': datetime.datetime.now() + datetime.timedelta(minutes=15)
            }
            encoded = jwt.encode(payload, APP_SECRET, algorithm="HS256")
            session.clear()
            redirect_uri = url_for('sessionImport', _external=True, _scheme='https', token=encoded)
            return render_template('export.html', redirect_uri=redirect_uri)
        else:
            return redirect('/')        

@app.route('/import')
def sessionImport():
    args = request.args
    token = args.get('token', None)
    try:
        parsed = jwt.decode(token, APP_SECRET, algorithms="HS256")
        if parsed['g_verified']:
            session['pass_gauth'] = parsed['g_verified']
            session['googler'] = parsed['googler']
            session['loogler'] = parsed['loogler']
            session['imported'] = True
        return redirect('/')
    except jwt.ExpiredSignatureError:
        return redirect('/')

@app.route('/verify')
def verify():
    redirect_uri = url_for('auth', _external=True, _scheme='https',)
    print(redirect_uri)
    return oauth.google.authorize_redirect(redirect_uri)

@app.route('/discord')
def discord():
    if 'pass_gauth' in session:
        redirect_uri = url_for('dauth', _external=True, _scheme='https',)
        return oauth.discord.authorize_redirect(redirect_uri)
    else:
        return redirect('/verify')

def revoke_token(token):
    url = f"https://oauth2.googleapis.com/revoke?token={token['access_token']}"
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    return requests.post(url=url, headers=headers)

@app.route('/auth')
def auth():
    token = oauth.google.authorize_access_token()
    if 'hd' in token['userinfo']:
        if token['userinfo']['hd'] in ['portal.google.com', 'google.com']:
            session['pass_gauth'] = True
            if token['userinfo']['hd'] in ["google.com"]:
                session['googler'] = True
            if token['userinfo']['hd'] in ["portal.google.com"]:
                session['loogler'] = True
            revoke_token(token)
            return redirect('/step2')
    revoke_token(token)
    return redirect('/noroute')

@app.route('/step2')
def step2():
    pass_gauth = session.get('pass_gauth', None)
    imported = session.get('imported', None)
    if pass_gauth:
        return render_template('step2.html', imported=imported)
    else:
        return redirect('/')

@app.route('/alt-verify')
def altVerify():
    return render_template('alt.html', email=SMTP_SERVER != False)

@app.route('/postAltVerify', methods=["POST"])
def reqAltVerify():
    import re
    user_email = request.form.get('email')
    x = re.search(r"([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@google\.com", user_email)
    if x:
        payload = {
            'domain': 'google.com',
            'exp': datetime.datetime.now() + datetime.timedelta(minutes=15)
        }

        encoded = jwt.encode(payload, APP_SECRET, algorithm="HS256")
        url = url_for('doAltVerify', _external=True, _scheme='https', token=encoded)
        message = f"""Hello,

        To verify your account, please follow this link: {url}
        """
        import smtplib, ssl
        from email.message import EmailMessage
        context = ssl.create_default_context()
        try:
            with smtplib.SMTP(SMTP_SERVER, SMTP_PORT) as server:
                server.ehlo()  # Can be omitted
                server.starttls(context=context)
                server.ehlo()  # Can be omitted
                server.login(SMTP_USER, SMTP_PASSWORD)

                msg = EmailMessage()
                msg.set_content(message)

                msg['Subject'] = 'Verify your email'
                msg['From'] = SMTP_USER
                msg['To'] = user_email
                server.send_message(msg)
        except Exception as e:
            print(e)
    return {"ok": True}      

@app.route('/email')
def doAltVerify():
    args = request.args
    token = args.get('token', None)
    try:
        parsed = jwt.decode(token, APP_SECRET, algorithms="HS256")
        if 'domain' in parsed and parsed['domain'] == "google.com":
            session['pass_gauth'] = True
            session['googler'] = True
            session['loogler'] = False
        return redirect('/')
    except jwt.ExpiredSignatureError as ex:
        return redirect('/')

@app.route('/dauth')
def dauth():
    token = oauth.discord.authorize_access_token()
    r = requests.get("https://discordapp.com/api/users/@me", headers={"Authorization":"Bearer {}".format(token['access_token'])})
    user = r.json()
    add_to_guild(token['access_token'], user['id'])
    if 'googler' in session and session['googler'] == True:
        add_to_role(token['access_token'], user['id'], DISCORD_ROLE_1)
    if 'loogler' in session and session['loogler'] == True:
        add_to_role(token['access_token'], user['id'], DISCORD_ROLE_2)
    send_message(user['id'], "Your account has been verified! You should now have access to protected channels.")
    session['pass_dauth'] = True
    return redirect('/welcome')

@app.route('/logout')
def logout():
    session.clear()
    return redirect('/')

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(500)
def page_not_found(e):
    return render_template('500.html'), 500


if __name__ == '__main__':
    app.run(host='0.0.0.0')