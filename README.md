# Athena for Discord

OAuth Verification for Discord Roles

## How it works

Your OAuth session is used only to verify your domain. For example, if signing in with a Google account, the token response contains a "userinfo" object, which contains the domain your account belongs to. This app verifies your account domain matches the expected domains. **Your OAuth token is not saved by Athena.** The only thing that is saved in your session is boolean values on whether you passed verification or not.

Once your domain is verified, you'll be directed to Discord to assign your roles. If you're not already in the server, you'll be automatically added with roles applied. Again, your Discord OAuth session is not retained once verified. Athena stores no relation between Discord and your OAuth identity.

## Want to use Athena for your server?

We'll be cleaning this up and exposing a config file to set your own OAuth steps for verification. 
